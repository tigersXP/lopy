from network import LoRa
import socket
import binascii
import struct
import time




from lib.MPL3115A2 import MPL3115A2
from lib.LTR329ALS01 import LTR329ALS01
from lib.SI7006A20 import SI7006A20
from network import LoRa


# Initialize LoRa in LORAWAN mode.
lora = LoRa(mode=LoRa.LORAWAN)


# create an OTAA authentication parameters
app_eui = binascii.unhexlify('70B3D57ED00086EE'.replace(' ',''))
app_key = binascii.unhexlify('3257838E9CD4B784B2D15560E990E82D'.replace(' ',''))
id = bytes([0x05]);

print(binascii.hexlify(lora.mac()))
lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)

# wait until the module has joined the network
while not lora.has_joined():
    time.sleep(2.5)
    print('Not yet joined...')

print('\n JOINED!')

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)

# make the socket blocking
# (waits for the data to be sent and for the 2 receive windows to expire)
s.setblocking(True)

# send some data
s.send(bytes([0x01, 0x02, 0x03]))

# make the socket non-blocking
# (because if there's no data received it will block forever...)
s.setblocking(False)

# get any data received (if any...)
data = s.recv(64)
print(data)

temp = MPL3115A2()
lux = LTR329ALS01()
multi = SI7006A20()
press = MPL3115A2()
while True:
    # Read data from the libraries and place into string
    data = "%.1f %.2f %.2f %.2f" % (temp.temperature(),press.pressure(), lux.light()[0], multi.humidity())
    luxStr = "%.1f" % lux.light()[0];

## Compression of temperature

    temp2=temp.temperature()
    temp2 = temp2*10+100
    tempb1=0
    tempb2=0
    if temp2 > 255:
        tempb2 = temp2 - 255
        tempb1 = 255
    else:
        tempb1 = temp.temperature()
        tempb2 =0

# ##
#     temp_mul = multi.temperature();
#     temp_mul = temp_mul*10+100
#     temp_mul1 = 0
#     temp_mul2 = 0
#     if temp_mul > 255:
#         temp_mul2 = temp_mul2 - 255
#         temp_mul1 = 255
#     else:
#         temp_mul1 = temp_mul2
#         temp_mul2 = 0

##
    #This is a value between 0 and 100% so it will always fit in 1 byte
    humi = multi.humidity();

    # #humi = 45
    # print(humi)
    # humi = humi*10+100
    # print(humi)
    # humi1 = 0
    # humi2 = 0
    # if humi > 255:
    #     print("hum>255")
    #     humi2 = humi-255
    #     print(humi2)
    #     humi1 = 255
    # else:
    #     humi1 = humi2
    #     print(humi1)
    #     humi2 = 0

##


    temp21 = int(tempb2+tempb1)
    tempbytes22 = bytearray(struct.pack("i",temp21));

    pressBytes = bytearray(struct.pack("f", press.pressure()));

    luxBytes = bytearray(struct.pack("f", float(luxStr)));

    humiiBytes = bytearray(struct.pack("b",int(humi)));


    print("Sending %s" % data)


    #s.send(id+tempBytes+temp2Bytes+luxBytes+humBytes)

    print(id)
    print(tempbytes22)
    print(humiiBytes)
    print(pressBytes)
    print(luxBytes)
    s.send(id+tempbytes22+humiiBytes+pressBytes+luxBytes)


    time.sleep(900)
